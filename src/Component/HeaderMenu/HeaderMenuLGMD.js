import React from "react";

import Navbar from "./Navbar";
export default function HeaderMenuLGMD() {

  return (
    <div className="text-cyan-900 font-medium py-1">
      <div className="flex justify-between items-center h-full">
        <a className="flex justify-center items-center" href="/">
          <img width={100} src={require("../../img/learning.png")} alt="" />
          <div className="text-left">
            <p
              className=" font-black text-red-600 lg:text-3xl md:text-2xl"
            >
              E-learning
            </p>
            <p
              style={{ wordSpacing: "0.3rem" }}
              className=" text-red-600 lg:text-sm md:text-xs"
            >
              Đào tạo chuyên gia lập trình
            </p>
          </div>
        </a>
        {/* <div
          className="block md:block lg:block "
          onClick={() => {
            window.location.href = "/findcourse";
          }}
        >
          <SearchCourse />
        </div> */}
        <ul className="flex space-x-5 lg:space-x-10 md:space-x-2 font-bold">
          <li>
            <a className="text-red-600" href="/">
              Trang chủ
            </a>
          </li>
          <li>
            <a className="text-red-600" href="/listcourse">
              Khóa Học
            </a>
          </li>
          <li className="text-red-600">
            <a href="/findcourse">Tìm Kiếm</a>
          </li>

          <li className="text-red-600">
            <a href="/infopage">Thông Tin</a>
          </li>
        </ul>
        <Navbar />
      </div>
    </div>
  );
}
