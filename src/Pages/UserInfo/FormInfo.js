import React, { useEffect, useState } from "react";
import { Button, Checkbox, Form, Input, message } from "antd";
import { useSelector } from "react-redux";
import { userLocalStoreChange } from "../../service/localService";
import { postUserInfo } from "../../service/userService";
import { putUserInfo } from "./../../service/userService";

export default function FormInfo() {
  const onFinish = (values) => {
    console.log("Success:", values);
    putUserInfo(values)
      .then((res) => {
        console.log("res", res);
        message.success("cập nhật thành công");
        setTimeout(() => {
          // window.location.reload();
        }, 1000);
      })
      .catch((err) => {
        console.log(err);
      });
  };

  const onFinishFailed = (errorInfo) => {
    console.log("Failed:", errorInfo);
  };
  let userChange = useSelector((state) => {
    return state.userSlice.user;
  });
  const [UserInfo, setUserInfo] = useState([]);

  useEffect(() => {
    postUserInfo(userChange.taiKhoan)
      .then((res) => {
        // console.log(res);
        setUserInfo(res.data);
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);

  return (
    <div>
      <Form
        fields={[
          {
            name: ["taiKhoan"],
            value: UserInfo.taiKhoan,
          },
          {
            name: ["matKhau"],
            value: UserInfo.matKhau,
          },
          {
            name: ["email"],
            value: UserInfo.email,
          },
          {
            name: ["soDT"],
            value: UserInfo.soDT,
          },
          {
            name: ["maNhom"],
            value: UserInfo.maNhom,
          },
          {
            name: ["maLoaiNguoiDung"],
            value: UserInfo.maLoaiNguoiDung,
          },
          {
            name: ["hoTen"],
            value: UserInfo.hoTen,
          },
          {
            name: ["maLoaiNguoiDung"],
            value: UserInfo.maLoaiNguoiDung,
          },
          {
            name: ["maNhom"],
            value: UserInfo.maNhom,
          },
        ]}
        name="basic"
        // labelCol={{
        //   span: 8,
        // }}
        // wrapperCol={{
        //   span: 24,
        // }}
        initialValues={{
          remember: true,
        }}
        onFinish={onFinish}
        onFinishFailed={onFinishFailed}
        autoComplete="off"
      >
        <Form.Item
          value="ấdfsadfsadf"
          label="Tài Khoản"
          name="taiKhoan"
          rules={[
            {
              message: "Please input your tài khoản!",
            },
          ]}
        >
          <Input disabled />
        </Form.Item>

        <Form.Item
          label="Mật Khẩu"
          name="matKhau"
          rules={[
            {
              message: "Please input your Mật Khẩu!",
            },
          ]}
        >
          <Input.Password />
        </Form.Item>

        <Form.Item
          label="Họ tên"
          name="hoTen"
          rules={[
            {
              message: "Please input your Họ tên!",
            },
          ]}
        >
          <Input />
        </Form.Item>

        <Form.Item
          label="email"
          name="email"
          rules={[
            {
              message: "Please input your email!",
            },
          ]}
        >
          <Input />
        </Form.Item>

        <Form.Item
          label="Số điện thoại"
          name="soDT"
          rules={[
            {
              message: "Please input your Số điện thoại!",
            },
          ]}
        >
          <Input />
        </Form.Item>

        <Form.Item
          label="Mã loại người dùng"
          name="maLoaiNguoiDung"
          rules={[
            {
              message: "Please input your Mã loại người dùng!",
            },
          ]}
        >
          <Input disabled />
        </Form.Item>

        <Form.Item
          label="Mã nhóm"
          name="maNhom"
          rules={[
            {
              message: "Please input your Mã nhóm!",
            },
          ]}
        >
          <Input disabled />
        </Form.Item>

        <Form.Item name="remember" valuePropName="checked"></Form.Item>

        <Form.Item>
          <button
            onClick={() => {
              onFinish();
            }}
            className="bg-green-500 px-3 py-1 rounded-md hover:brightness-75"
            htmlType="submit"
          >
            Cập Nhật
          </button>
        </Form.Item>
      </Form>
    </div>
  );
}
