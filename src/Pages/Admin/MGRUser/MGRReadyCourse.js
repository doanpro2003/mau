import React, { useEffect, useState } from "react";
import { message, Space, Table, Tag } from "antd";
import { useParams } from "react-router-dom";
import {
  postDeleteCourse,
  postReadyCours,
} from "./../../../service/courseService";
export default function MGRReadyCourse() {
  const columns = [
    {
      title: "Mã Khóa Học",
      dataIndex: "maKhoaHoc",
      key: "maKhoaHoc",
    },
    {
      title: "Tên Khóa Học",
      dataIndex: "tenKhoaHoc",
      key: "tenKhoaHoc",
    },
    {
      title: "Hành động",
      dataIndex: "action",
      key: "action",
    },
  ];

  let param = useParams();
  const [readyCourse, setreadyCourse] = useState([]);
  //   console.log("readyCourse", readyCourse);
  useEffect(() => {
    postReadyCours({ taiKhoan: param.tk })
      .then((res) => {
        // console.log(res);
        setreadyCourse(res.data);
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);
  const renderReadyCourse = () => {
    return readyCourse.map((item) => {
      return {
        ...item,
        action: (
          <button
            onClick={() => {
              postDeleteCourse({
                maKhoaHoc: item.maKhoaHoc,
                taiKhoan: param.tk,
              })
                .then((res) => {
                  message.success("xóa thành công");
                  setTimeout(() => {
                    window.location.reload();
                  }, 1000);
                  console.log(res);
                })
                .catch((err) => {
                  message.error("xóa thất bại");
                  console.log(err);
                });
            }}
            className="bg-red-500 border-2 border-black px-2 py-1 rounded-md"
          >
            hủy
          </button>
        ),
      };
    });
  };
  return (
    <div>
      <p>KHÓA HỌC ĐÃ GHI DANH</p>
      <Table columns={columns} dataSource={renderReadyCourse()} />;
    </div>
  );
}
